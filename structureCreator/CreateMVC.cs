﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace structureCreator
{
    /// <summary>
    /// this form allows to create all files for MVC structure 
    /// in directory our choice
    /// </summary>
    public class CreateMVC : Form
    {
        private StructureCreator _structureCreator;
        private List<string> _listPages;
        private GroupBox _grpMVC;
        private string _wampPath = @"C:\wamp\www";
        private string _wamp64Path = @"C:\wamp64\www";
        private string _directoryPath;
        private RadioButton _chkWamp;
        private RadioButton _chkWamp64;
        private RadioButton _chkOther;
        private CheckBox _chkBootstrap;
        private CheckBox _chkJquery;
        private TextBox _txtOther;
        private TextBox _txtProjectName;
        private Button _cmdCreate;
        private Label _lblProjectName;
        private StreamWriter _index;
        /// <summary>
        /// builder of our form
        /// </summary>
        /// <param name="structureCreator"></param>
        public CreateMVC(StructureCreator structureCreator)
        {
            int posX = 13;

            this._structureCreator = structureCreator;
            this._listPages = _structureCreator.lstViews;
            #region this form
            this.Text = "MVC emplacement";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Size = new Size(300, 300);
            #endregion this form
            #region GroupBox
            this._grpMVC = new GroupBox();
            this._grpMVC.Text = "Emplacements: ";
            this._grpMVC.Location = new Point(posX, 13);
            this._grpMVC.Size = new Size(this.Width - 39, this.Height - this._grpMVC.Location.Y * 2 - 39);
            #endregion GroupBox
            #region Labels
            this._lblProjectName = new Label();
            this._lblProjectName.Text = "Nom du projet : ";
            this._lblProjectName.Location = new Point(posX, 150);
            this._lblProjectName.Width = 80;
            #endregion Labels
            #region RadioButtons
            this._chkWamp = new RadioButton();
            this._chkWamp.Checked = true;
            this._chkWamp.Location = new Point(posX, 13);
            this._chkWamp.Text = "Wamp :     " + this._wampPath;
            this._chkWamp.Width = 200;
            this._chkWamp.Click += new
                System.EventHandler(this._chkWamp_Click);

            this._chkWamp64 = new RadioButton();
            this._chkWamp64.Location = new Point(posX, 39);
            this._chkWamp64.Text = "Wamp64 : " + this._wamp64Path;
            this._chkWamp64.Width = 200;
            this._chkWamp64.Click += new
                System.EventHandler(this._chkWamp_Click);

            this._chkOther = new RadioButton();
            this._chkOther.Location = new Point(posX, 65);
            this._chkOther.Text = "Autre :     ";
            this._chkOther.Width = 100;
            this._chkOther.Click += new
                System.EventHandler(this._chkOther_Click);
            #endregion CheckBox
            #region CheckBoxes
            this._chkBootstrap = new CheckBox();
            this._chkBootstrap.Checked = true;
            this._chkBootstrap.Location = new Point(posX, 100);
            this._chkBootstrap.Text = "Bootstrap";
            this._chkBootstrap.Width = 75;

            this._chkJquery = new CheckBox();
            this._chkJquery.Checked = true;
            this._chkJquery.Location = new Point(posX + 150, 100);
            this._chkJquery.Text = "Jquery";
            this._chkJquery.Width = 75;
            #endregion CheckBoxes
            #region TextBox
            this._txtOther = new TextBox();
            this._txtOther.ReadOnly = true;
            this._txtOther.Location = new Point(this._chkOther.Width - 23, 65);
            this._txtOther.Width = this._grpMVC.Width - this._chkOther.Width;

            this._txtProjectName = new TextBox();
            this._txtProjectName.ReadOnly = false;
            this._txtProjectName.Location = new Point(this._lblProjectName.Width + 13, 147);
            this._txtProjectName.Width = 147;
            #endregion TextBox
            #region Buttons
            this._cmdCreate = new Button();
            this._cmdCreate.Location = new Point(78, 200);
            this._cmdCreate.Text = "Créer la structure MVC";
            this._cmdCreate.Width = 100;
            this._cmdCreate.Click += new System.EventHandler(this._cmdCreate_Click);
            #endregion Buttons
            this._grpMVC.Controls.Add(this._chkWamp);
            this._grpMVC.Controls.Add(this._chkWamp64);
            this._grpMVC.Controls.Add(this._txtOther);
            this._grpMVC.Controls.Add(this._chkOther);
            this._grpMVC.Controls.Add(this._cmdCreate);
            this._grpMVC.Controls.Add(this._lblProjectName);
            this._grpMVC.Controls.Add(this._txtProjectName);
            this._grpMVC.Controls.Add(this._chkBootstrap);
            this._grpMVC.Controls.Add(this._chkJquery);
            this.Controls.Add(this._grpMVC);
        }
        private void _chkWamp_Click(object sender, EventArgs e)
        {
            this._chkOther.Checked = false;
            this._txtOther.Clear();
        }
        private void _chkOther_Click(object sender, EventArgs e)
        {
            if (this._chkOther.Checked)
            {
                this._chkWamp.Checked = false;
                FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                folderBrowserDialog.ShowDialog();
                this._directoryPath = folderBrowserDialog.SelectedPath;
                this._txtOther.Text = this._directoryPath;
                if (this._txtOther.Text == "")
                {
                    _chkWamp.Checked = true;
                }
            }
            else
            {
                this._txtOther.Clear();
            }
        }
        private void _cmdCreate_Click(object sender, EventArgs e)
        {
            #region InitSwitch
            int rdiSwitch = 0;
            if (_chkWamp.Checked) rdiSwitch = 1;
            else if (_chkWamp64.Checked) rdiSwitch = 2;
            else rdiSwitch = 3;

            switch (rdiSwitch)
            {
                case 1:
                    createStructure(@"C:\wamp\www");
                    break;
                case 2:
                    createStructure(@"C:\wamp64\www");
                    break;
                case 3:
                    createStructure(_txtOther.Text);
                    break;
            }
            #endregion InitSwitch 
        }
        private void createStructure(string path)
        {
            #region CreationDeBase
            //Création du contenu de base
            //Cette variable définit le chemin de base pour le dossier wamp
            #region createDirectory
            string rep = path + @"\" + _txtProjectName.Text;
            //On créer les dossier de la structure MVC dans le répertoire spécifié au dessus
            System.IO.Directory.CreateDirectory(rep + @"\contents");
            System.IO.Directory.CreateDirectory(rep + @"\views");
            System.IO.Directory.CreateDirectory(rep + @"\controller");
            System.IO.Directory.CreateDirectory(rep + @"\model");
            #endregion createDirectory
            //On fait un try catch pour attrapper toutes les exceptions
            try
            {
                #region modele
                //Fichier modele
                // Instanciation du StreamWriter avec passage du nom du fichier 
                StreamWriter modele = new StreamWriter(rep + @"\model\model.php");
                // ou bien 
                // StreamWriter monStreamWriter = new StreamWriter(Server.MapPath("./") + "admin\\logs\\" + fichier); 

                //Ecriture du texte dans le fichier modele 
                modele.WriteLine(@"<?php");
                modele.WriteLine("//your functions");
                modele.WriteLine("?>");

                // Fermeture du StreamWriter (Très important) 
                modele.Close();
                #endregion modele
                #region gabarit
                //variable contenant le code du gabarit
                string htmlCode = @"<html>
    <head>
        <title><?= $title ?></title>
        <meta charset='utf-8'>";
                //Si on ne veut pas intégrer bootstrap et jquery
                if (_chkBootstrap.Checked)
                {
                    htmlCode += @"
        <!-- Latest compiled and minified CSS -->
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'>

        <!-- Optional theme -->
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css' integrity='sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp' crossorigin='anonymous' >";
                }
                htmlCode += @"
    </head>
    <body>
        <?= $content ?>";
                if (_chkJquery.Checked)
                {
                    htmlCode += @"
        <script src='http://code.jquery.com/jquery-latest.min.js' type='text/javascript'></script>";
                }
                if (_chkBootstrap.Checked)
                {
                    htmlCode += @"
        <!-- Latest compiled and minified JavaScript -->
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' integrity='sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa' crossorigin='anonymous'></script>";
                }
                htmlCode += @"
    </body>
</html>";
                //Fichier gabarit
                //Instanciation du StreamWriter avec passage du nom du fichier
                StreamWriter gabarit = new StreamWriter(rep + @"\views\gabarit.php");
                //Ecriture du texte dans le fichier gabarit
                gabarit.Write(htmlCode);
                //Fermeture du fichier gabarit
                gabarit.Close();
                #endregion gabarit
                #region controleur
                //fichier controleur
                //Instanciation du StreamWriter avec passage du nom du fichier
                StreamWriter controleur = new StreamWriter(rep + @"\controller\controller.php");
                //Ecriture dans le fichier controleur
                controleur.Write(@"<?php
    require 'model/model.php';
    
    function home()
    {
        require 'views/home.php';
    }

    function error($message)
    {
        require 'views/error.php';
    }
");
                //Fermeture du fichier controleur
                controleur.Close();
                #endregion controleur
                #region index
                //Fichier index
                //Instanciation du StreamWriter avec passage du nom du fichier
                _index = new StreamWriter(rep + @"\index.php");
                //Ecriture dans le fichier index
                _index.Write(@"<?php
    session_start();
    require 'controller/controller.php';
    try
    {
        if(isset($_GET['action']))
        {
            switch($_GET['action'])
            {
                case 'home':
                    home();
                    break;
                case 'error':
                    error();
                    break;
                ");
                //fermeture du fichier index
                _index.Close();
                #endregion index
                #region accueil
                //Fichier accueil
                //Instanciation du StreamWriter avec passage du nom du fichier
                StreamWriter accueil = new StreamWriter(rep + @"\views\home.php");
                //Ecriture dans le fichier accueil
                accueil.Write(@"<?php
    $title='home.php';
    ob_start();
?>
<h1>home.php</h1>
<?php
    $content=ob_get_clean();
    require 'gabarit.php';
?>");
                //Fermeture du fichier acueil
                accueil.Close();
                #endregion accueil
                #region erreur
                //Fichier erreur
                //Instanciation du StreamWriter avec passage du nom du fichier
                StreamWriter erreur = new StreamWriter(rep + @"\views\error.php");
                //Ecriture dans le fichier erreur
                erreur.Write(@"<?php
    $title='error.php';
    ob_start();
?>
<h1><?= $message ?></h1>
<?php
    $content=ob_get_clean();
    require 'gabarit.php';
?>");
                //Fermeture du fichier erreur
                erreur.Close();
                #endregion erreur
            }
            catch (Exception ex)
            {
                // Code exécuté en cas d'exception 
                MessageBox.Show(ex.ToString(), "erreur");
            }
            #endregion CreationDeBase
            #region FichiersUtilisateur
            foreach (string element in _listPages)
            {
                try
                {
                    #region createPages
                    //Création des pages de la liste retournées par le formulaire pages
                    // On instancie le fichier récuperé dans la liste
                    StreamWriter creationPages = new StreamWriter(rep + @"\views\" + element);
                    //On écrit dans le fichier
                    creationPages.Write(@"<?php
    $title='" + element + @"';
    ob_start();
?>
<h1>" + element + @"</h1>
<?php
    $content=ob_get_clean();
    require 'gabarit.php';
?>");
                    //On ferme le fichier
                    creationPages.Close();
                    #endregion createPages
                    #region addToControleur
                    //On instancie le fichier controleur. Le param true permet d'écrire à la suite du fichier déjà existant
                    StreamWriter controleur = new StreamWriter(rep + @"\controller\controller.php", true);
                    //On écrit dans le fichier controleur
                    controleur.Write(@"
    function " + element.Substring(0, element.LastIndexOf(".")) + @"()
    {
        require 'views/" + element + @"';
    }
    
");
                    //On ferme le fichier controleur
                    controleur.Close();
                    #endregion addToControleur
                    #region addToIndex
                    //On instancie le fichier index. Le param true permet dd'écrire à la suite du fichier
                    _index = new StreamWriter(rep + @"\index.php", true);
                    //On écrit dans le fichier
                    _index.Write(@"
                case '" + element.Substring(0, element.LastIndexOf('.')) + @"':
                    " + element.Substring(0, element.LastIndexOf(".")) + @"();
                    break;");
                    //On ferme le fichier
                    _index.Close();
                    #endregion addToIndex
                }
                catch (Exception ex)
                {
                    // Code exécuté en cas d'exception 
                    MessageBox.Show(ex.ToString(), "erreur");
                }
            }
            #endregion FichiersUtilisateur
            #region FermetureIndex
            //On instancie le fichier index. Le param true permet d'écrire à la suite du fichier
            StreamWriter index = new StreamWriter(rep + @"\index.php", true);
            //On écrit dans le fichier index
            index.Write(@"
                default:
                    error('La page que vous cherchez est introuvable.');
                    break;
            }
        }
        else
        {
            home();
        }
    }
    catch(exception $e)
    {
        echo $e->getMessage();
    }
?>");
            //On ferme le fichier index
            index.Close();
            #endregion FermetureIndex
            //Si tout à bien été, on affiche que la structure à été crée
            MessageBox.Show("La création de votre infrastrucutre à été effectuée", "Réussite!");
        }
    }
}
