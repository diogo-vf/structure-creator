﻿/*
 * Creator: Diogo Vieira Ferreira et Yannick Tercier
 * Cration Date: 01.10.2017
 * Version: 1.0
 * Description: Creates a mvc structure and also allows the creation of a bootstrap structure
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace structureCreator
{
    /// <summary>
    /// it's a principal form
    /// </summary>
    public partial class StructureCreator : Form
    {
        #region Variables
        private int _posX = 13;
        private TabControl _tabControl;
        //choice
        private TabPage _tabPageChoice;
        private CheckBox _chkMVC;
        private CheckBox _chkBootstrap;
        private CheckBox _chkDelete;
        //mvc
        private TabPage _tabPageMVC;
        private ListBox _cboNewViews;
        private Label _lblView;
        private List<string> _lstViews;
        private Button _cmdAddView;
        private Button _cmdRemoveView;
        private Button _cmdValidateViews;
        //bootstrap
        private TabPage _tabPageBootstarp;
        private Label[] _lblCol;
        private List<int> _lstBootstrap;
        private TabControl _tabControlBootstrap;
        private TabPage _tabPageAffichageBtstp;
        private TabPage _tabPageCodeBtstp;
        private RichTextBox _rtfCodeBtstp;
        private TextBox[] _txtColTempBtstp;
        private int _ligne = 0;
        #endregion Variables
        /// <summary>
        /// create a form with your components
        /// </summary>
        public StructureCreator()
        {
            InitializeComponent();
            this.Text = "Createur de structures";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;

            this._tabControl = new TabControl();
            this._tabPageChoice = new TabPage();
            this._chkMVC = new CheckBox();
            this._chkBootstrap = new CheckBox();
            this._chkDelete = new CheckBox();
            //creation area of tabControl
            #region tabControl
            this._tabControl.Size = new Size(259, 126);
            this._tabControl.Location = new Point(this._posX, 13);
            this._tabControl.SelectedIndex = 0;
            this._tabControl.TabIndex = 0;
            #endregion tabControl

            //
            // content choice tab page
            //

            //creation first page and content
            #region tab01Choice
            this._tabPageChoice.Text = "Choix";
            this._tabPageChoice.Enter += new
                System.EventHandler(this._tabPageChoice_Enter);
            #endregion tab01Choice
            #region checkbox 
            this._chkMVC.Text = "Structure MVC";
            this._chkMVC.Click += new
                System.EventHandler(this._chkMVC_checked);
            this._chkMVC.Location = new Point(this._posX, 13);
            this._chkBootstrap.Text = "Structure Bootstrap";
            this._chkBootstrap.Width = 200;
            this._chkBootstrap.Location = new Point(this._posX, 13*3);
            this._chkBootstrap.Click += new
                System.EventHandler(this._chkBootstrap_checked);
            this._chkDelete.Text = "Effacer les données à chaque \nchangement de page?";
            this._chkDelete.Height = 40;
            this._chkDelete.Width = 200;
            this._chkDelete.Checked = true;
            this._chkDelete.Location = new Point(this._posX, 13*5);
            #endregion checkbox 
            //add content first page
            this._tabPageChoice.Controls.Add(this._chkMVC);
            this._tabPageChoice.Controls.Add(this._chkBootstrap);
            this._tabPageChoice.Controls.Add(this._chkDelete);
            //add tabControl to the form
            this.Controls.Add(this._tabControl);
            //add pages to the tabControl
            this._tabControl.Controls.Add(this._tabPageChoice);
            //creation second tab to the tabcontrol
            #region tab02MVC
            this._tabPageMVC = new TabPage();
            this._tabPageMVC.Text = "MVC";
            this._tabPageMVC.Size = new Size(259, 236);
            this._tabPageMVC.Enter += new
                System.EventHandler(this._tabPageMVC_Enter);
            #endregion tab02MVC
            //creation third tab to the tabcontrol
            #region tab Bootstarp
            this._tabPageBootstarp = new TabPage();
            this._tabPageBootstarp.Text = "Bootstrap";
            this._tabPageBootstarp.Size = new Size(259, 236);
            this._tabPageBootstarp.Enter += new
                System.EventHandler(this._tabPageBootstarp_Enter);
            #endregion tab Bootstarp

            //
            // content mvc tab page
            //
            int lstWidth = 200;
            #region label
            this._lblView = new Label();
            this._lblView.Text = "Veuillez écrire les vues que vous désirez:";
            this._lblView.Width = 200;
            this._lblView.Location = new Point(this._posX, 13);
            #endregion label
            #region buttom
            this._cmdAddView = new Button();
            this._cmdAddView.Text = "Ajouter";
            int posYcmd = 326 - this._cmdAddView.Height * 2 - 39;
            this._cmdAddView.Location = new Point(this._posX, posYcmd);
            this._cmdAddView.Click += new
                System.EventHandler(this._cmdAddView_Click);

            this._cmdRemoveView = new Button();
            this._cmdRemoveView.Text = "Effacer";
            this._cmdRemoveView.Location = new Point(this._posX + lstWidth - this._cmdRemoveView.Width, posYcmd);
            this._cmdRemoveView.Click += new
                System.EventHandler(this._cmdRemoveView_Click);

            this._cmdValidateViews = new Button();
            this._cmdValidateViews.Text = "Valider";
            this._cmdValidateViews.Location = new Point(this._posX + this._cmdAddView.Width - 12, posYcmd + 29);
            this._cmdValidateViews.Click += new
                System.EventHandler(this._cmdValidateViews_Click);
            #endregion buttom
            #region listBox
            this._cboNewViews = new ListBox();
            this._cboNewViews.Location = new Point(this._posX, 39);
            this._cboNewViews.Width = lstWidth;
            this._cboNewViews.Height = this._cmdAddView.Location.Y - this._cmdAddView.Height - 23;
            this._cboNewViews.SelectionMode = SelectionMode.MultiExtended;
            #endregion listBox
            this._tabPageMVC.Controls.Add(this._lblView);
            this._tabPageMVC.Controls.Add(this._cboNewViews);
            this._tabPageMVC.Controls.Add(this._cmdAddView);
            this._tabPageMVC.Controls.Add(this._cmdRemoveView);
            this._tabPageMVC.Controls.Add(this._cmdValidateViews);


            //
            // content bootstrap tab page
            //
            #region label
            Label lblInfo1 = new Label();
            lblInfo1.Name = "lblInfo1";
            lblInfo1.Text = "cliquez sur les colonnes rouges pour ajouter";
            lblInfo1.Location = new Point(this._posX, 13);
            lblInfo1.Width = 500;
            lblInfo1.Font = new Font("Verdana", 10, FontStyle.Bold);
            this._tabPageBootstarp.Controls.Add(lblInfo1);
            this._tabControlBootstrap = new TabControl();
            this._tabControlBootstrap.Size = new Size(500, 500);
            this._tabControlBootstrap.Location = new Point(500 + this._posX * 3, 13);
            this.Controls.Add(this._tabControlBootstrap);
            //page where user views his structure bootstrap graphically
            this._tabPageAffichageBtstp = new TabPage();
            this._tabPageAffichageBtstp.Text = "Affichage";
            this._tabControlBootstrap.Controls.Add(this._tabPageAffichageBtstp);
            //page where user views his structure bootstrap in code
            this._tabPageCodeBtstp = new TabPage();
            this._tabPageCodeBtstp.Text = "Code";
            this._tabControlBootstrap.Controls.Add(this._tabPageCodeBtstp);
            //where show bootstrap code
            this._rtfCodeBtstp = new RichTextBox();
            this._rtfCodeBtstp.Location = new Point(this._posX, 13);
            this._rtfCodeBtstp.Size = new Size(this._tabControlBootstrap.Width - 39, this._tabControlBootstrap.Height - 52);
            this._tabPageCodeBtstp.Controls.Add(this._rtfCodeBtstp);
            #endregion label
            //creation of bars for compare with those of bootstrap
            this._lblCol = new Label[13];
            for (int i = 1; i <= 12; i++)
            {
                #region label
                this._lblCol[i] = new Label();
                int font = 7;
                if (i != 1)
                    font = 10;
                this._lblCol[i].Name = "col" + i;
                this._lblCol[i].Font = new Font("Verdana", font, FontStyle.Bold);
                this._lblCol[i].Text = "col-xs-" + i;
                this._lblCol[i].Width = i * (500 - 33) / 12;
                this._lblCol[i].Height = 30;
                int posY = 43;//under lblInfo
                if (i != 1)
                    posY = 35 * i + 10;
                this._lblCol[i].Location = new Point(this._posX, posY);
                this._lblCol[i].BackColor = Color.Red;
                this._lblCol[i].Click += new
                    System.EventHandler(this._lblCol_click);
                #endregion label
                this._tabPageBootstarp.Controls.Add(this._lblCol[i]);
            }
        }
        private void viderVariablesGlobales()
        {
            if (this._lstViews != null)
            {
                this._lstViews = null;
                this._cboNewViews.Items.Clear();
            }
            if (this._lstBootstrap != null)
            {
                this._lstBootstrap = null;
                this._rtfCodeBtstp.Clear();
                this._tabPageAffichageBtstp.Controls.Clear();
            }
        }
        //
        //
        //  tabPage Choice
        //
        //
        private void _tabPageChoice_Enter(object sender, EventArgs e)
        {
            this._tabControl.Size = new Size(200, 136);
            this.Height = 200;
            this.Width = 240;
            this.CenterToScreen();
        }
        private void _chkMVC_checked(object sender, EventArgs e)
        {
            if (this._chkMVC.Checked)
            {
                this._tabControl.Controls.Add(this._tabPageMVC);
            }
            else
            {
                this._tabControl.Controls.Remove(this._tabPageMVC);
            }
        }
        private void _chkBootstrap_checked(object sender, EventArgs e)
        {
            if (this._chkBootstrap.Checked)
            {
                this._tabControl.Controls.Add(this._tabPageBootstarp);
            }
            else
            {
                this._tabControl.Controls.Remove(this._tabPageBootstarp);
            }
        }
        //
        //
        //  tabPage MVC
        //
        //
        private void _tabPageMVC_Enter(object sender, EventArgs e)
        {
            //create all components on tab02
            this._tabControl.Size = new Size(259, 326);
            this.Height = this._tabControl.Location.Y + this._tabControl.Height + 62;
            this.Width = 300;
            this.CenterToScreen();
            if (this._chkDelete.Checked)
                viderVariablesGlobales();
        }
        private void _cmdAddView_Click(object sender, EventArgs e)
        {
            AddView addView = new AddView(this);
            addView.Show();
        }
        private void _cmdValidateViews_Click(object sender, EventArgs e)
        {
            CreateMVC createMVC = new CreateMVC(this);
            createMVC.Show();
        }
        private void _cmdRemoveView_Click(object sender, EventArgs e)
        {
            if (this._cboNewViews.SelectedIndex != -1)
            {
                while (this._cboNewViews.SelectedIndex != -1)
                {
                    int index = this._cboNewViews.SelectedIndex;
                    this._cboNewViews.Items.RemoveAt(index);
                }
                this._lstViews.Clear();
                foreach(string views in this._cboNewViews.Items)
                {
                    this._lstViews.Add(views);
                }
            }
            else
            {
                MessageBox.Show("Veuillez selectionner un élèment dans la liste SVP", "Aucun élèment sélectionné", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Add content in listBox _cboNewViews
        /// </summary>
        /// <param name="newView">content a add</param>
        public void addNewView( string newView)
        {
            if (this._lstViews == null)
                this._lstViews = new List<string>();
            bool OK = true;
            foreach (string resultat in this._lstViews)
            {
                if (resultat == newView)
                    OK = false;
            }
            if (OK)
                this._lstViews.Add(newView);

            if (this._lstViews != null)
            {
                this._cboNewViews.Items.Clear();
                foreach (string text in this._lstViews)
                {
                    this._cboNewViews.Items.Add(text);
                }
            }
        }
        /// <summary>
        /// return the list with all views for mvc
        /// </summary>
        public List<string> lstViews
        {
            get
            {
                if (_lstViews == null)
                    _lstViews = new List<string>();
                return this._lstViews;
            }
        }
        //
        //
        //  tabPage Bootstrap
        //
        //
        private void _tabPageBootstarp_Enter(object sender, EventArgs e)
        {
            this._tabControl.Size = new Size(500, 500);
            this.Height = 572;
            this.Width = 1065;
            this.CenterToScreen();
            if (this._chkDelete.Checked)
                viderVariablesGlobales();
        }
        private void _lblCol_click(object sender, EventArgs e)
        {
            if(_ligne<11)
            {
                int size = 0;
                //retrieves the data of the label on which we have clicked
                Label clickedLabe = sender as Label;
                switch (clickedLabe.Name)
                {
                    case "col1":
                        size = 1;
                        break;
                    case "col2":
                        size = 2;
                        break;
                    case "col3":
                        size = 3;
                        break;
                    case "col4":
                        size = 4;
                        break;
                    case "col5":
                        size = 5;
                        break;
                    case "col6":
                        size = 6;
                        break;
                    case "col7":
                        size = 7;
                        break;
                    case "col8":
                        size = 8;
                        break;
                    case "col9":
                        size = 9;
                        break;
                    case "col10":
                        size = 10;
                        break;
                    case "col11":
                        size = 11;
                        break;
                    case "col12":
                        size = 12;
                        break;
                }
                if (this._lstBootstrap == null)
                    this._lstBootstrap = new List<int>();
                this._lstBootstrap.Add(size);
                AfficheBootstrap();
            }
            else
            {
                MessageBox.Show("Vous ne pouvez pas en ajouter plus, veuillez en supprimer pour continuer", "Limite atteinte", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void AfficheBootstrap()
        {
            Label lblInfo2 = new Label();
            lblInfo2.Name = "lblInfo2";
            lblInfo2.Text = "cliquez sur les colonnes rouges pour l'enlever";
            lblInfo2.Location = new Point(this._posX, 13);
            lblInfo2.Width = 500;
            lblInfo2.Font = new Font("Verdana", 10, FontStyle.Bold);
            this._tabPageAffichageBtstp.Controls.Add(lblInfo2);
            int posX;
            int posY;
            _ligne = 0;
            int colonne = 0;
            int i = 0;
            this._txtColTempBtstp   = new TextBox[this._lstBootstrap.Count];
            foreach (int num in this. _lstBootstrap)
            {
                colonne += num;
                if (colonne > 12)
                {
                    _ligne++;
                    colonne = num;
                }
                if (colonne - num == 0 || i == 0)
                {
                    posX = _posX;
                }
                else
                {
                    posX = this._txtColTempBtstp[i - 1].Location.X + this._txtColTempBtstp[i - 1].Width;
                }
                posY = 35 * _ligne + 36;
                this._txtColTempBtstp[i] = new TextBox();
                this._txtColTempBtstp[i].Click += new
                    System.EventHandler(this._txtColTempBtstp_enleve_click);
                this._txtColTempBtstp[i].Text = this._lblCol[num].Text;
                this._txtColTempBtstp[i].Name = i.ToString();
                this._txtColTempBtstp[i].MinimumSize = this._lblCol[num].Size;
                this._txtColTempBtstp[i].MaximumSize = this._lblCol[num].Size;
                this._txtColTempBtstp[i].BackColor = this._lblCol[num].BackColor;
                this._txtColTempBtstp[i].Location = new Point(posX, posY);
                this._tabPageAffichageBtstp.Controls.Add(this._txtColTempBtstp[i]);
                i++;
            }
            GenerateHtml();
        }
        private void _txtColTempBtstp_enleve_click(object sender, EventArgs e)
        {
            //retrieves the data of the label on which we have clicked
            TextBox clickedTextBox = sender as TextBox;
            for(int i=0;i<=_lstBootstrap.Count-1;i++)
            {
                if(i.ToString()==clickedTextBox.Name)
                {
                    _lstBootstrap.RemoveAt(i);
                }
            }
            _tabPageAffichageBtstp.Controls.Clear();
            AfficheBootstrap();
        }
        private void GenerateHtml()
        {
            string htmlCode = null;
            foreach (int num in this._lstBootstrap)
            {
                htmlCode += "<div class='col-xs-" + num + "'>Col " + num + @"</div>
";
            }
            this._rtfCodeBtstp.Text = htmlCode;
            if (htmlCode != null)
                Clipboard.SetText(htmlCode);//ajoute au presse papier
            else
                Clipboard.Clear();
        }
    }
}
