﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace structureCreator
{
    /// <summary>
    /// it's a form for add view in the listBox of the MVC tabPage
    /// </summary>
    public class AddView : Form
    {
        #region Variables
        private Label _lblAddView;
        private TextBox _txtAddView;
        private Button _cmdAddView;
        private Button _cmdCloseView;
        private StructureCreator _form;
        #endregion Variables
        /// <summary>
        /// create a form for add text in list of his parent
        /// </summary>
        /// <param name="form">his parent</param>
        public AddView(StructureCreator form)
        {
            //place all components on form
            int posX = 20;
            this._form = form;
            #region this form
            this.Text = "Ajouter vue";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Height = 150;
            this.Width = 250;
            #endregion this form
            #region label
            this._lblAddView = new Label();
            this._lblAddView.Text = "Nom de la vue à ajouter:";
            this._lblAddView.Location = new Point(posX, 13);
            this._lblAddView.Width = 200;
            #endregion label
            #region textBox
            this._txtAddView = new TextBox();
            this._txtAddView.Location = new Point(posX, 39);
            this._txtAddView.Width = 200;
            this._txtAddView.KeyPress += new
                System.Windows.Forms.KeyPressEventHandler(this._txtAddView_KeyPress); 
            #endregion textBox
            #region Buttom
            this._cmdAddView = new Button();
            int posYcmd = this._lblAddView.Height + this._lblAddView.Location.Y + this._cmdAddView.Height + 13;
            this._cmdAddView.Text = "Valider";
            this._cmdAddView.Click += new
                System.EventHandler(_cmdAddView_Click);
            this._cmdAddView.Location = new Point(posX, posYcmd);

            this._cmdCloseView = new Button();
            this._cmdCloseView.Text = "Fermer";
            this._cmdCloseView.Click += new
                System.EventHandler(_cmdCloseView_Click);
            int posXcmd = this.Width - this._cmdCloseView.Width - posX - 10;
            this._cmdCloseView.Location = new Point(posXcmd, posYcmd);
            #endregion Buttom
            this.Controls.Add(this._lblAddView);
            this.Controls.Add(this._txtAddView);
            this.Controls.Add(this._cmdAddView);
            this.Controls.Add(this._cmdCloseView);
        }
        private void _txtAddView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                checkData();
        }
        private void _cmdAddView_Click(object sender, EventArgs e)
        {
            checkData();
        }
        private void checkData()
        {
            try
            {
                if (_txtAddView.Text.IndexOf(".php") != -1)
                    this._txtAddView.Text = this._txtAddView.Text.Substring(0, (this._txtAddView.Text.Length - 4));
                if (!string.IsNullOrWhiteSpace(this._txtAddView.Text) && !string.IsNullOrEmpty(this._txtAddView.Text))
                {
                    if (int.TryParse(this._txtAddView.Text, out int intResult) || double.TryParse(this._txtAddView.Text, out double doubleResult))
                    {
                        MessageBox.Show("Veuillez entrer des lettres et non des chiffres", "Valeurs numériques", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        bool OK = true;
                        //compare the word and ignore the case sensitive
                        if (String.Compare(this._txtAddView.Text, "home", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(this._txtAddView.Text, "error", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(this._txtAddView.Text, "gabarit", StringComparison.OrdinalIgnoreCase) == 0 || String.Compare(this._txtAddView.Text, "index", StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            OK = false;
                        }
                        else
                        {
                            this._txtAddView.Text = this._txtAddView.Text.Replace(" ", "_");//replace all spaces per underscores
                            foreach (string resultat in this._form.lstViews)
                            {
                                if (String.Compare(this._txtAddView.Text, resultat, StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    OK = false;
                                    break;
                                }
                            }
                        }
                        if (OK)
                        {
                            this._form.addNewView(this._txtAddView.Text += ".php");
                        }
                        else
                        {
                            MessageBox.Show("Vous essayez d'ajouter une vue qui existe déjà", "Problème détecté", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez Remplir la texte box SVP", "Aucun Texte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Veuillez Remplir la texte box SVP avec + de 1 lettre", "Aucun Texte", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            this._txtAddView.Clear();
            this._txtAddView.Focus();
        }
        private void _cmdCloseView_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
